# CASC applied to Medicare Referral Networks

Pipeline for executing CASC codes on CMS referral network datasets using zipcode covariates

## Installation

TODO: Describe the installation process

## Usage

TODO: Write usage instructions

## Execution
To run on peach.stat.wisc.edu long term, follow the following instructions:
. ssh peach.stat.wisc.edu
. stashticket
. screen
. ssh peach
. <run your script/code here>
. Ctrl-A d
. exit

To bring up the screen running your scripts, run the following:
ssh peach.stat.wisc.edu
screen -r

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History

TODO: Write history

## Pkg Dependencies
`
install.packages("Matrix", contriburl="http://cran.rstudio.com/src/contrib/")   
install.packages("igraph", contriburl="http://cran.rstudio.com/src/contrib/") 
install.packages("data.table", contriburl="http://cran.rstudio.com/src/contrib/") 
install.packages("rARPACK", contriburl="http://cran.rstudio.com/src/contrib/") 
install.packages("zipcode", contriburl="http://cran.rstudio.com/src/contrib/") 
install.packages("deldir", contriburl="http://cran.rstudio.com/src/contrib/") 
install.packages("datautils", contriburl="http://cran.rstudio.com/src/contrib/") 
install.packages("maps", contriburl="http://cran.rstudio.com/src/contrib/")
install.packages("grid", contriburl="http://cran.rstudio.com/src/contrib/") 
install.packages("spatgraphs", contriburl="http://cran.rstudio.com/src/contrib/") 
install.packages("acs", contriburl="http://cran.rstudio.com/src/contrib/") 
`
### Parallel Execution
`
install.packages("iterators", contriburl="http://cran.rstudio.com/src/contrib/")
install.packages("foreach", contriburl="http://cran.rstudio.com/src/contrib/")
install.packages("parallel", contriburl="http://cran.rstudio.com/src/contrib/")
install.packages("doMC", contriburl="http://cran.rstudio.com/src/contrib/")
`




## Credits

TODO: Write credits

## License

TODO: Write license
