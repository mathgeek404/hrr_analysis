# Ensemble Execution Script

source("src/CASCZipMain.R")

### File inputs ###
pwd = "data/2015_30d/"
data.referral <- "physician-shared-patient-patterns-2015-days30.txt"
data.physician.compare <- "../National_Downloadable_File.csv"
data.physician.nppes <- "../npidata_20050523-20160207.csv"
data.zip.crosswalk <- "../ZipHsaHrr13.csv"

### Parameters ###
casc.clusters <- 305
restarts.n <- 10000
casc.tuning <- 1
preprocess.files <- FALSE

CASC_main(pwd, data.referral, data.physician.compare,
               data.physician.nppes, data.zip.crosswalk,
               casc.clusters, restarts.n, casc.tuning, preprocess.files)


### File inputs ###
pwd = "data/2015_60d/"
data.referral <- "physician-shared-patient-patterns-2015-days60.txt"

### Parameters ###
casc.clusters <- 305
restarts.n <- 1000
casc.tuning <- 1
preprocess.files <- TRUE

#CASC_main(pwd, data.referral, data.physician.compare,
#               data.physician.nppes, data.zip.crosswalk,
#               casc.clusters, restarts.n, casc.tuning, preprocess.files)


### File inputs ###
pwd = "data/2015_90d/"
data.referral <- "physician-shared-patient-patterns-2015-days90.txt"

### Parameters ###
casc.clusters <- 305
restarts.n <- 1000
casc.tuning <- 1
preprocess.files <- TRUE

#CASC_main(pwd, data.referral, data.physician.compare,
#               data.physician.nppes, data.zip.crosswalk,
#               casc.clusters, restarts.n, casc.tuning, preprocess.files)

### File inputs ###
pwd = "data/2015_180d/"
data.referral <- "physician-shared-patient-patterns-2015-days180.txt"

### Parameters ###
#casc.clusters <- 305
#restarts.n <- 1000
#casc.tuning <- 1
#preprocess.files <- TRUE

#CASC_main(pwd, data.referral, data.physician.compare,
#                  data.physician.nppes, data.zip.crosswalk,
#                  casc.clusters, restarts.n, casc.tuning, preprocess.files)
