This folder contains a data folder that I have used to store “intermediate” data files.  

YaleBioStat.pdf		Contains my slides about HCC’s.  

RUNcasc.R		this is the “master” file that calls the “make” files to run CASC.

makeKnn.R  		constructs the k-nearest neighbor graph on zipcodes.  This is perhaps casc specific.  

makeNPIpop.R		defines the population of NPI’s.  “Data cleaning” should happen in this file… for example, removing NPI’s that move zipcodes or focusing on NPI’s of a specific specialty or removing NPI’s that bill from outside the lower 48 states. That can happen in this file. 

makezippop.R		defines the population of zipcodes for the analyis.  In particular, CASC does not rquire that all zipcodes have a referral… they can enter the analysis because they are in the knn graph.  

makeZiprefgraph.R	constructs the referral network between zipcodes.

plotRegions.R		If you want to plot regions of zipcodes.  This contains some fun stuff.  for example, it uses a voronoi tesselation of zipcodes.  Lots of fun stuff about tesselations and coloring graphs. 

